package com.epam.games;

public class LongestPlateau {
    private int start;
    private int end;
    private int length;
    private Findable findable;
    private int[] array;

    public LongestPlateau(int[] array) {
        this.array = array;
        findPlateau(array);
    }

    public int getLength() {
        return length;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    private void findPlateau(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] < array[i + 1]) {
                int tmp_length = 1;
                for (int j = i + 1; j < array.length - 1; j++) {
                    if (array[j] != array[j + 1]) {
                        if (array[j] > array[j + 1] && tmp_length > length) {
                            length = tmp_length;
                            start = i + 1;
                            end = j + 1;
                        }
                        //i = j;
                        break;
                    } else {
                        tmp_length++;
                    }
                }
            }
        }
    }

    int[] findViaFindable() {
        return findable.find(array);
    }
}
