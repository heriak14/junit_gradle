package com.epam.games;

import org.junit.Test;
import org.junit.jupiter.api.RepeatedTest;

import static org.junit.Assert.*;

public class MinesweeperTest {

    private static final boolean[][] testField = {
            {false, true, true, false},
            {true, true, true, true},
            {false, true, false, true},
            {false, true, true, true}};
    private static final int[][] result = {
            {3, 1, 1, 3},
            {1, 1, 1, 1},
            {4, 1, 8, 1},
            {2, 1, 1, 1}};
    private Minesweeper minesweeper;

    @Test
    @RepeatedTest(5)
    public void testFillField() {
        minesweeper = new Minesweeper(4, 4, 0.75);
        assertNotNull(minesweeper.getField());
    }

    @Test
    public void testGetSolution() {
        minesweeper = new Minesweeper(testField);
        assertArrayEquals(result, minesweeper.getSolution());
    }

    @Test
    public void testCountNeighbourMines() {
        minesweeper = new Minesweeper(testField);
        assertSame(3, minesweeper.countNeighboursMines(0, 0));
        assertSame(3, minesweeper.countNeighboursMines(0, 3));
        assertSame(8, minesweeper.countNeighboursMines(2, 2));
    }

    @Test
    public void testGetInt() {
        Minesweeper ms = new Minesweeper(5, 5, 0.75);
        assertEquals(1, ms.getInt(true));
        assertEquals(0, ms.getInt(false));
    }
}
